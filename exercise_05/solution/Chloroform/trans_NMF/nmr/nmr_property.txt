-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1886332331
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999986677 
   Number of Beta  Electrons                 15.9999986677 
   Total number of  Electrons                31.9999973355 
   Exchange energy                          -21.4751581572 
   Correlation energy                        -1.2755069663 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7506651235 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1886332331 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                4.9000000000
     Refrac:                 1.4500000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                573
     Surface Area:         360.3742021631
     Dielectric Energy:     -0.0121047754
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8273     6.0000     0.1727     4.2038     4.2038    -0.0000
  1   0     8.4346     8.0000    -0.4346     2.0592     2.0592     0.0000
  2   0     7.1892     7.0000    -0.1892     3.0795     3.0795     0.0000
  3   0     6.1766     6.0000    -0.1766     3.8211     3.8211    -0.0000
  4   0     0.9104     1.0000     0.0896     0.9955     0.9955    -0.0000
  5   0     0.8138     1.0000     0.1862     0.9961     0.9961    -0.0000
  6   0     0.8863     1.0000     0.1137     1.0218     1.0218    -0.0000
  7   0     0.8811     1.0000     0.1189     0.9860     0.9860     0.0000
  8   0     0.8807     1.0000     0.1193     0.9860     0.9860    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                1.939455
                0             6               2            7                1.247798
                0             6               4            1                0.979940
                2             7               3            6                0.878302
                2             7               5            1                0.955493
                3             6               6            1                0.970698
                3             6               7            1                0.974334
                3             6               8            1                0.974408
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.188633
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.9561181044
        Electronic Contribution:
                  0    
      0      -2.906555
      1      -0.386415
      2      -0.972436
        Nuclear Contribution:
                  0    
      0       4.570493
      1      -0.010471
      2       0.036649
        Total Dipole moment:
                  0    
      0       1.663937
      1      -0.396887
      2      -0.935788
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      24.911393  -0.598318  -1.401184
      1       1.367704  20.408451   2.006966
      2       3.325901   1.990599  24.386395
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.002744   0.722728  -0.691127
      1      -0.923223  -0.267403  -0.275964
      2       0.384256  -0.637307  -0.667972
 P Eigenvalues: 
                  0          1          2    
      0      19.577608  24.022221  26.106411
P(iso)  23.235413
 Nucleus: 5 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.082050  -0.087712  -0.126031
      1      -0.993065  22.863476   2.696551
      2      -2.356794   2.702399  28.252422
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.004030   0.834240   0.551386
      1      -0.923982   0.207756  -0.321085
      2       0.382416   0.510765  -0.769985
 P Eigenvalues: 
                  0          1          2    
      0      21.743759  27.228904  30.225284
P(iso)  26.399316
 Nucleus: 6 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.266781   0.321941   0.750618
      1      -0.678139  24.634933   2.443723
      2      -1.707114   2.471943  29.816229
 P Tensor eigenvectors:
                   0          1          2    
      0       0.002656   0.977156   0.212506
      1      -0.928632   0.081249  -0.361996
      2       0.370993   0.196379  -0.907634
 P Eigenvalues: 
                  0          1          2    
      0      23.654501  28.169167  30.894274
P(iso)  27.572647
 Nucleus: 7 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.286601   4.940360   1.079098
      1       5.665963  31.513257  -0.773772
      2       1.315846   0.517214  27.203270
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.732592   0.235822  -0.638512
      1       0.578918  -0.277509  -0.766709
      2       0.358000   0.931331  -0.066779
 P Eigenvalues: 
                  0          1          2    
      0      24.586309  27.549244  35.867574
P(iso)  29.334376
 Nucleus: 8 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.379583  -2.783453   4.199793
      1      -3.147227  29.492459  -1.417142
      2       4.944419  -2.722526  29.116900
 P Tensor eigenvectors:
                   0          1          2    
      0       0.727208  -0.229364   0.646962
      1       0.173183  -0.850722  -0.496267
      2      -0.664210  -0.472932   0.578930
 P Eigenvalues: 
                  0          1          2    
      0      24.579522  27.571676  35.837744
P(iso)  29.329647
 Nucleus: 0 C  
 Shielding tensor (ppm): 
                  0          1          2    
      0     -10.733173 -22.405417 -54.557833
      1     -17.711562  75.746078 -42.316865
      2     -43.439225 -42.307460  -8.336633
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.849825   0.527052  -0.003789
      1       0.206477   0.326295  -0.922442
      2       0.484938   0.784696   0.386117
 P Eigenvalues: 
                  0          1          2    
      0      32.469055 -69.145883  93.353100
P(iso)  18.892091
 Nucleus: 3 C  
 Shielding tensor (ppm): 
                  0          1          2    
      0     146.435009  10.223747  24.886772
      1       9.041046 165.852412   1.238342
      2      22.225924   1.172401 168.524501
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.840682  -0.002538  -0.541523
      1       0.205872  -0.926405  -0.315262
      2       0.500869   0.376520  -0.779335
 P Eigenvalues: 
                  0          1          2    
      0     129.876110 165.390485 185.545327
P(iso)  160.270641
 Nucleus: 2 N  
 Shielding tensor (ppm): 
                  0          1          2    
      0     198.433278  25.241202  60.854334
      1       7.192712 144.412832 -49.357984
      2      17.933707 -48.896265  46.872227
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.325973   0.003728  -0.945372
      1       0.363987   0.923397  -0.121865
      2       0.872499  -0.383827  -0.302360
 P Eigenvalues: 
                  0          1          2    
      0      18.439771 164.866257 206.412309
P(iso)  129.906112
 Nucleus: 1 O  
 Shielding tensor (ppm): 
                  0          1          2    
      0     -275.288624  42.175689  97.791883
      1      18.764574 279.424410 -193.095849
      2      37.463226 -191.620418 -99.834950
 P Tensor eigenvectors:
                   0          1          2    
      0       0.540557   0.840975   0.023635
      1       0.323318  -0.233592   0.917006
      2       0.776701  -0.488052  -0.398172
 P Eigenvalues: 
                  0          1          2    
      0     -140.629952 -314.573529 359.504316
P(iso)  -31.899722
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.134570348317   -0.010770749633    0.040048068963
               1 O      0.398184176344    0.362219255998    0.928644933848
               2 N      2.489928064176    0.002378876801    0.080403055866
               3 C      3.249811062310    0.471181644254    1.224158193586
               4 H      0.754590163704   -0.408861237495   -0.917279625040
               5 H      2.981280520439   -0.334134245321   -0.729657107788
               6 H      2.541952528046    0.774840715550    1.990436890312
               7 H      3.873187089015    1.326373291616    0.958227019753
               8 H      3.886206047650   -0.320547551769    1.622548570501
