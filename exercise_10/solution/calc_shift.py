import numpy as np

# Isotropic shielding of TMS
tms_H=31.7

# Isotropic shielding of phenanthrene
H=[  23.732,  23.803,  23.486,  22.477,  23.626,  23.626,  22.479,  23.733,  23.802,  23.486]
delta = tms_H - np.array(H)
print(delta)

# Isotropic shielding of phenanthrene_acetyl
H = [  23.303, 23.920, 23.513, 23.603, 23.666, 20.198, 23.724, 23.746, 23.480, 28.016]
delta = tms_H - np.array(H)
print(delta)
