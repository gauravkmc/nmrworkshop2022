-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1768587460
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000056459 
   Number of Beta  Electrons                 16.0000056459 
   Total number of  Electrons                32.0000112919 
   Exchange energy                          -21.4710608765 
   Correlation energy                        -1.2747112080 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7457720846 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1768587460 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8431     6.0000     0.1569     4.2610     4.2610     0.0000
  1   0     8.3423     8.0000    -0.3423     2.1523     2.1523     0.0000
  2   0     7.1701     7.0000    -0.1701     3.0360     3.0360     0.0000
  3   0     6.1728     6.0000    -0.1728     3.8702     3.8702     0.0000
  4   0     0.9522     1.0000     0.0478     1.0032     1.0032     0.0000
  5   0     0.8444     1.0000     0.1556     1.0297     1.0297     0.0000
  6   0     0.9022     1.0000     0.0978     1.0037     1.0037     0.0000
  7   0     0.8874     1.0000     0.1126     0.9816     0.9816     0.0000
  8   0     0.8854     1.0000     0.1146     0.9725     0.9725     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.045858
                0             6               2            7                1.194601
                0             6               4            1                0.980492
                2             7               3            6                0.930406
                2             7               5            1                0.946564
                3             6               6            1                0.976013
                3             6               7            1                0.974053
                3             6               8            1                0.969159
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.176859
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.2352112891
        Electronic Contribution:
                  0    
      0      -3.344605
      1      -1.201779
      2      -2.801842
        Nuclear Contribution:
                  0    
      0       4.721367
      1       1.633681
      2       3.635087
        Total Dipole moment:
                  0    
      0       1.376762
      1       0.431902
      2       0.833245
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      24.817521   1.126291   2.288365
      1      -0.097283  21.181573   1.296004
      2      -2.265123   0.509978  24.357145
 P Tensor eigenvectors:
                   0          1          2    
      0       0.126424  -0.422398   0.897550
      1      -0.960671   0.173382   0.216911
      2       0.247242   0.889673   0.383866
 P Eigenvalues: 
                  0          1          2    
      0      20.880603  24.538308  24.937329
P(iso)  23.452080
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.368381  -0.272281  -5.369123
      1       0.441371  23.395411   0.413392
      2      -1.063601   1.541545  30.363292
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.139942  -0.820109  -0.554831
      1       0.970055  -0.225900   0.089235
      2      -0.198519  -0.525729   0.827163
 P Eigenvalues: 
                  0          1          2    
      0      23.189270  26.155980  32.781835
P(iso)  27.375695
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.624748  -0.117297   0.471637
      1      -0.917726  26.902336   2.875912
      2      -1.353463   2.433010  30.968185
 P Tensor eigenvectors:
                   0          1          2    
      0       0.130410   0.980444  -0.147385
      1       0.896911  -0.053309   0.438986
      2      -0.422544   0.189440   0.886323
 P Eigenvalues: 
                  0          1          2    
      0      25.556083  27.579379  32.359807
P(iso)  28.498423
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.904937   4.514964   1.094151
      1       4.921166  29.994113  -0.836183
      2       1.928859   0.577158  27.090076
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.661522   0.235291  -0.712058
      1       0.607242  -0.389099  -0.692718
      2       0.440051   0.890640  -0.114519
 P Eigenvalues: 
                  0          1          2    
      0      24.652977  27.544951  34.791198
P(iso)  28.996375
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.460997  -3.041285   4.240780
      1      -2.686917  29.008358  -1.241833
      2       4.663175  -2.548063  28.374977
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.693572   0.260686   0.671566
      1      -0.147934   0.860821  -0.486931
      2       0.705035   0.437070   0.558477
 P Eigenvalues: 
                  0          1          2    
      0      24.364847  27.221284  35.258201
P(iso)  28.948111
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -3.786316   6.701592  68.223104
      1       5.940356  73.563266 -10.912640
      2      57.625686 -12.172093 -13.561495
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.882914  -0.469510   0.004799
      1      -0.118898   0.213676  -0.969642
      2      -0.454232   0.856681   0.244482
 P Eigenvalues: 
                  0          1          2    
      0      45.841972 -63.541729  73.915212
P(iso)  18.738485
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     152.615161   7.691095  25.286839
      1       4.958646 149.673928   5.856808
      2      25.285865   7.140559 164.132170
 P Tensor eigenvectors:
                   0          1          2    
      0       0.786695   0.107840   0.607850
      1      -0.054142  -0.968779   0.241944
      2      -0.614963   0.223246   0.756294
 P Eigenvalues: 
                  0          1          2    
      0     132.389508 147.513669 186.518082
P(iso)  155.473753
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     213.763884 -14.213498 -92.235779
      1       2.728671 146.638039 -37.515858
      2     -39.893176 -26.967611  63.243604
 P Tensor eigenvectors:
                   0          1          2    
      0       0.426301  -0.151764  -0.891760
      1       0.269240   0.962434  -0.035083
      2       0.863584  -0.225142   0.451147
 P Eigenvalues: 
                  0          1          2    
      0      31.689142 155.092346 236.864038
P(iso)  141.215175
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -298.281496 -101.094737 -69.162400
      1     -81.355166 255.428605 -135.642594
      2     -19.865587 -128.978212 -203.448763
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.554882  -0.170803  -0.814207
      1       0.149649   0.942240  -0.299648
      2       0.818359  -0.288114  -0.497271
 P Eigenvalues: 
                  0          1          2    
      0     -199.200783 298.002738 -345.103608
P(iso)  -82.100551
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2120
          Total Spin-Spin Coupling ISO:  30.9264 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3554
          Total Spin-Spin Coupling ISO:  10.7916 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4835
          Total Spin-Spin Coupling ISO:  4.1055 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1056
          Total Spin-Spin Coupling ISO:  182.7831 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0126
          Total Spin-Spin Coupling ISO:  -0.4739 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5912
          Total Spin-Spin Coupling ISO:  6.9408 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2570
          Total Spin-Spin Coupling ISO:  5.7654 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.1321
          Total Spin-Spin Coupling ISO:  1.0493 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2800
          Total Spin-Spin Coupling ISO:  0.3081 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 3.6109
          Total Spin-Spin Coupling ISO:  0.6549 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0337
          Total Spin-Spin Coupling ISO:  -6.9712 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4965
          Total Spin-Spin Coupling ISO:  1.5193 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.7990
          Total Spin-Spin Coupling ISO:  0.6182 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.2937
          Total Spin-Spin Coupling ISO:  -0.1852 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.2043
          Total Spin-Spin Coupling ISO:  -0.3112 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4478
          Total Spin-Spin Coupling ISO:  8.6750 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0475
          Total Spin-Spin Coupling ISO:  13.9649 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0084
          Total Spin-Spin Coupling ISO:  63.0683 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0770
          Total Spin-Spin Coupling ISO:  -0.6802 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0976
          Total Spin-Spin Coupling ISO:  0.1573 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1184
          Total Spin-Spin Coupling ISO:  0.1860 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.6029
          Total Spin-Spin Coupling ISO:  1.7639 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1316
          Total Spin-Spin Coupling ISO:  5.2394 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0892
          Total Spin-Spin Coupling ISO:  128.6439 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0904
          Total Spin-Spin Coupling ISO:  133.2882 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0926
          Total Spin-Spin Coupling ISO:  136.7778 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9378
          Total Spin-Spin Coupling ISO:  9.1286 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2453
          Total Spin-Spin Coupling ISO:  0.0031 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.4669
          Total Spin-Spin Coupling ISO:  -0.6349 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.2320
          Total Spin-Spin Coupling ISO:  -0.5721 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9762
          Total Spin-Spin Coupling ISO:  10.2182 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.4475
          Total Spin-Spin Coupling ISO:  3.1794 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.6244
          Total Spin-Spin Coupling ISO:  -0.2621 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7589
          Total Spin-Spin Coupling ISO:  -13.2800 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7709
          Total Spin-Spin Coupling ISO:  -11.1781 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7728
          Total Spin-Spin Coupling ISO:  -8.7849 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.095674877050   -0.086770778862    0.153979117175
               1 O      0.367633568638   -0.420842519383   -0.755570117849
               2 N      2.436493438940    0.086303248986    0.057583408582
               3 C      3.297898253338    0.478343614301    1.153275062578
               4 H      0.720437418495    0.115633329247    1.174089943802
               5 H      2.836227238788   -0.062594452770   -0.856200699952
               6 H      2.684896240103    0.855428149958    1.970871778680
               7 H      3.969039258895    1.281325335805    0.846972537720
               8 H      3.898599705753   -0.354885927282    1.525658969264
