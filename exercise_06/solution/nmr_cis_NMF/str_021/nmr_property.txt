-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1764959237
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000045823 
   Number of Beta  Electrons                 16.0000045823 
   Total number of  Electrons                32.0000091647 
   Exchange energy                          -21.4709155197 
   Correlation energy                        -1.2746468337 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7455623534 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1764959237 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8413     6.0000     0.1587     4.2600     4.2600     0.0000
  1   0     8.3421     8.0000    -0.3421     2.1522     2.1522     0.0000
  2   0     7.1725     7.0000    -0.1725     3.0298     3.0298    -0.0000
  3   0     6.1753     6.0000    -0.1753     3.8721     3.8721    -0.0000
  4   0     0.9527     1.0000     0.0473     1.0027     1.0027     0.0000
  5   0     0.8436     1.0000     0.1564     1.0298     1.0298    -0.0000
  6   0     0.8985     1.0000     0.1015     0.9997     0.9997    -0.0000
  7   0     0.8851     1.0000     0.1149     0.9713     0.9713    -0.0000
  8   0     0.8889     1.0000     0.1111     0.9849     0.9849     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.046083
                0             6               2            7                1.192545
                0             6               4            1                0.981565
                2             7               3            6                0.927429
                2             7               5            1                0.947358
                3             6               6            1                0.977202
                3             6               7            1                0.968661
                3             6               8            1                0.975396
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.176496
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.2416152637
        Electronic Contribution:
                  0    
      0      -3.349929
      1      -1.220502
      2      -2.792522
        Nuclear Contribution:
                  0    
      0       4.736455
      1       1.566926
      2       3.654061
        Total Dipole moment:
                  0    
      0       1.386526
      1       0.346424
      2       0.861539
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      24.890821   1.017379   2.230798
      1      -0.846488  21.447613   1.289312
      2      -2.055032   1.480511  23.867178
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.007936  -0.306445  -0.951855
      1       0.909695   0.393025  -0.134117
      2      -0.415202   0.866962  -0.275653
 P Eigenvalues: 
                  0          1          2    
      0      20.818109  24.466064  24.921439
P(iso)  23.401871
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.542359  -2.784298  -4.760855
      1      -0.425400  24.815740   3.454162
      2      -0.860641   2.695975  28.751063
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.020331   0.822531  -0.568357
      1      -0.883431   0.251390   0.395414
      2       0.468119   0.510143   0.721539
 P Eigenvalues: 
                  0          1          2    
      0      23.132105  26.127370  32.849687
P(iso)  27.369721
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.149670   0.950872   0.560871
      1       0.613977  25.715886   0.358264
      2      -1.488821   0.601712  32.630123
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.407464   0.908579  -0.091968
      1       0.908288   0.413656   0.062463
      2      -0.094796   0.058082   0.993801
 P Eigenvalues: 
                  0          1          2    
      0      25.318425  27.485048  32.692206
P(iso)  28.498560
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.639302   4.505716   1.683818
      1       4.054993  32.582974  -0.107035
      2       1.874362   1.177205  26.544174
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.764815  -0.396460   0.507816
      1       0.365970   0.381346   0.848906
      2       0.530211  -0.835102   0.146567
 P Eigenvalues: 
                  0          1          2    
      0      24.362192  27.137466  35.266792
P(iso)  28.922150
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      31.794893  -2.609243   3.045616
      1      -2.268511  27.874304   0.466313
      2       4.292726  -0.671778  27.402989
 P Tensor eigenvectors:
                   0          1          2    
      0       0.526770   0.064406   0.847564
      1       0.409651   0.854450  -0.319532
      2      -0.744781   0.515526   0.423715
 P Eigenvalues: 
                  0          1          2    
      0      24.831844  27.650025  34.590317
P(iso)  29.024062
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -7.425528  27.970788  61.874736
      1      21.080384  58.561546 -36.307805
      2      52.851840 -36.636084   4.262533
 P Tensor eigenvectors:
                   0          1          2    
      0       0.861080  -0.489324  -0.138212
      1       0.326384   0.323483   0.888162
      2       0.389890   0.809889  -0.438253
 P Eigenvalues: 
                  0          1          2    
      0      44.400427 -65.194501  76.192625
P(iso)  18.466184
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     152.164889   9.958030  23.942685
      1      13.312306 151.981720   9.432947
      2      22.413982   7.572990 162.824946
 P Tensor eigenvectors:
                   0          1          2    
      0       0.800319   0.027055   0.598964
      1      -0.244756  -0.897213   0.367563
      2      -0.547343   0.440768   0.711435
 P Eigenvalues: 
                  0          1          2    
      0     132.760966 147.351157 186.859433
P(iso)  155.657185
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     220.181580 -38.352176 -80.747987
      1     -21.046437 136.956692 -36.796215
      2     -29.732273 -40.400963  71.475747
 P Tensor eigenvectors:
                   0          1          2    
      0       0.406797   0.025642  -0.913159
      1       0.393017   0.897454   0.200284
      2       0.824654  -0.440361   0.355004
 P Eigenvalues: 
                  0          1          2    
      0      34.619673 154.702725 239.291622
P(iso)  142.871340
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -313.690099 -39.950097 -88.447635
      1     -27.209392 204.696464 -211.179793
      2     -37.409425 -210.024905 -147.475005
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.572500   0.044437  -0.818700
      1       0.321946   0.930515  -0.174624
      2       0.754052  -0.363549  -0.547026
 P Eigenvalues: 
                  0          1          2    
      0     -200.993803 298.871652 -354.346489
P(iso)  -85.489547
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2121
          Total Spin-Spin Coupling ISO:  31.0062 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3551
          Total Spin-Spin Coupling ISO:  10.7766 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4834
          Total Spin-Spin Coupling ISO:  3.9981 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1057
          Total Spin-Spin Coupling ISO:  182.3417 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0124
          Total Spin-Spin Coupling ISO:  -0.5793 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.6219
          Total Spin-Spin Coupling ISO:  6.2509 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.0839
          Total Spin-Spin Coupling ISO:  0.1476 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2855
          Total Spin-Spin Coupling ISO:  7.4440 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2796
          Total Spin-Spin Coupling ISO:  0.3232 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 3.6111
          Total Spin-Spin Coupling ISO:  0.6186 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0337
          Total Spin-Spin Coupling ISO:  -6.9575 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4961
          Total Spin-Spin Coupling ISO:  1.5109 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.8220
          Total Spin-Spin Coupling ISO:  0.5189 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.1722
          Total Spin-Spin Coupling ISO:  -0.3054 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.3109
          Total Spin-Spin Coupling ISO:  -0.1160 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4491
          Total Spin-Spin Coupling ISO:  8.6351 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0480
          Total Spin-Spin Coupling ISO:  13.9626 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0082
          Total Spin-Spin Coupling ISO:  63.3166 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0840
          Total Spin-Spin Coupling ISO:  -0.6360 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1232
          Total Spin-Spin Coupling ISO:  0.1912 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0895
          Total Spin-Spin Coupling ISO:  0.1081 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.6027
          Total Spin-Spin Coupling ISO:  1.7056 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1329
          Total Spin-Spin Coupling ISO:  5.2552 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0894
          Total Spin-Spin Coupling ISO:  129.7237 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0928
          Total Spin-Spin Coupling ISO:  137.4079 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0896
          Total Spin-Spin Coupling ISO:  132.0571 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9380
          Total Spin-Spin Coupling ISO:  9.3270 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.3021
          Total Spin-Spin Coupling ISO:  -0.0213 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.1391
          Total Spin-Spin Coupling ISO:  -0.5014 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.5264
          Total Spin-Spin Coupling ISO:  -0.6244 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9628
          Total Spin-Spin Coupling ISO:  9.1040 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.6842
          Total Spin-Spin Coupling ISO:  -0.6102 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.3951
          Total Spin-Spin Coupling ISO:  4.6640 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7739
          Total Spin-Spin Coupling ISO:  -10.2889 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7562
          Total Spin-Spin Coupling ISO:  -13.6616 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7710
          Total Spin-Spin Coupling ISO:  -9.5675 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.087432692531    0.017972900397    0.117343470525
               1 O      0.363958748005   -0.386645208240   -0.767061040547
               2 N      2.441991416398   -0.009608354188    0.090907942257
               3 C      3.296156237351    0.474484685698    1.156726396820
               4 H      0.695507568574    0.455567893668    1.054111372750
               5 H      2.859440583063   -0.426960041953   -0.726382671611
               6 H      2.747397364725    0.461795682122    2.097786927943
               7 H      3.658437675150    1.489286049360    0.974903628916
               8 H      4.156577714201   -0.183953606864    1.272323972947
