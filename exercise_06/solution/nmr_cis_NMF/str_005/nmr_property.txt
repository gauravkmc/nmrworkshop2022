-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1767001489
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999880934 
   Number of Beta  Electrons                 15.9999880934 
   Total number of  Electrons                31.9999761867 
   Exchange energy                          -21.4708853114 
   Correlation energy                        -1.2746676895 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7455530010 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1767001489 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8419     6.0000     0.1581     4.2603     4.2603     0.0000
  1   0     8.3426     8.0000    -0.3426     2.1519     2.1519    -0.0000
  2   0     7.1709     7.0000    -0.1709     3.0332     3.0332     0.0000
  3   0     6.1734     6.0000    -0.1734     3.8711     3.8711     0.0000
  4   0     0.9526     1.0000     0.0474     1.0029     1.0029    -0.0000
  5   0     0.8442     1.0000     0.1558     1.0299     1.0299     0.0000
  6   0     0.8883     1.0000     0.1117     0.9834     0.9834     0.0000
  7   0     0.8857     1.0000     0.1143     0.9715     0.9715     0.0000
  8   0     0.9006     1.0000     0.0994     1.0020     1.0020    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.045442
                0             6               2            7                1.193966
                0             6               4            1                0.980874
                2             7               3            6                0.928737
                2             7               5            1                0.947027
                3             6               6            1                0.974938
                3             6               7            1                0.968673
                3             6               8            1                0.976710
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.176700
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.2400872317
        Electronic Contribution:
                  0    
      0      -3.396275
      1      -1.053999
      2      -2.800542
        Nuclear Contribution:
                  0    
      0       4.703536
      1       1.617929
      2       3.669887
        Total Dipole moment:
                  0    
      0       1.307261
      1       0.563930
      2       0.869345
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      23.683234   0.682339   2.582730
      1       2.569866  22.682158   0.376715
      2      -0.721990  -2.444081  23.927357
 P Tensor eigenvectors:
                   0          1          2    
      0       0.553630  -0.054194   0.830998
      1      -0.722918  -0.526621   0.447280
      2      -0.413381   0.848371   0.330731
 P Eigenvalues: 
                  0          1          2    
      0      20.856490  24.512212  24.924046
P(iso)  23.430916
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      25.654369   3.365303  -1.692364
      1       1.100328  27.743802  -5.563387
      2       0.949440  -2.644728  28.717765
 P Tensor eigenvectors:
                   0          1          2    
      0       0.553155   0.804327  -0.216972
      1      -0.689745   0.296118  -0.660731
      2      -0.467195   0.515142   0.718580
 P Eigenvalues: 
                  0          1          2    
      0      23.160232  26.154211  32.801492
P(iso)  27.371978
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.724889   2.365795   2.769751
      1       2.059337  31.722110   1.903034
      2       2.966545   3.619784  27.571453
 P Tensor eigenvectors:
                   0          1          2    
      0       0.655116   0.616903   0.436182
      1       0.078705  -0.629902   0.772676
      2      -0.751418   0.471863   0.461212
 P Eigenvalues: 
                  0          1          2    
      0      24.740776  27.585441  34.692235
P(iso)  29.006151
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      33.966342  -3.307420   0.960816
      1      -3.077446  26.244025   1.239082
      2       1.971735   0.382525  26.587741
 P Tensor eigenvectors:
                   0          1          2    
      0       0.337591   0.068991  -0.938761
      1       0.794258   0.514344   0.323426
      2      -0.505159   0.854805  -0.118841
 P Eigenvalues: 
                  0          1          2    
      0      24.349086  27.191269  35.257752
P(iso)  28.932702
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.338983   0.360842   0.867840
      1       0.805932  25.848983  -0.453004
      2      -0.607232  -1.874027  32.304351
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.304012   0.952642  -0.007003
      1       0.936844   0.297620  -0.183700
      2       0.172916   0.062408   0.982957
 P Eigenvalues: 
                  0          1          2    
      0      25.456730  27.529423  32.506164
P(iso)  28.497439
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      41.535318 -41.612338  27.371072
      1     -34.912884   9.077913  65.964395
      2      20.425412  60.593690   5.425510
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.876442  -0.188324   0.443151
      1      -0.277209  -0.555174  -0.784179
      2      -0.393706   0.810133  -0.434373
 P Eigenvalues: 
                  0          1          2    
      0      45.156640 -64.382563  75.264663
P(iso)  18.679580
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     162.801493  -1.594716  24.476102
      1      -4.291966 140.612878   7.498711
      2      22.256669   7.873819 163.103680
 P Tensor eigenvectors:
                   0          1          2    
      0       0.482907   0.537148   0.691573
      1       0.689010  -0.720489   0.078489
      2      -0.540431  -0.438598   0.718029
 P Eigenvalues: 
                  0          1          2    
      0     132.476998 147.467554 186.573500
P(iso)  155.506017
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     159.573233  56.453174 -86.900968
      1      37.916537 196.218165 -19.898255
      2     -48.498013  14.640679  69.741268
 P Tensor eigenvectors:
                   0          1          2    
      0       0.561349   0.572057   0.598029
      1      -0.058976  -0.693135   0.718391
      2       0.825475  -0.438537  -0.355354
 P Eigenvalues: 
                  0          1          2    
      0      32.879005 154.853156 237.800504
P(iso)  141.844222
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -144.184877 -234.344759 -197.385229
      1     -248.072904  41.187435 111.069919
      2     -156.548346 141.921045 -147.223648
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.258218  -0.608752  -0.750163
      1      -0.608721   0.705488  -0.362968
      2       0.750188   0.362915  -0.552730
 P Eigenvalues: 
                  0          1          2    
      0     -200.003123 297.826610 -348.044578
P(iso)  -83.407030
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2122
          Total Spin-Spin Coupling ISO:  31.0684 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3551
          Total Spin-Spin Coupling ISO:  10.8378 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4842
          Total Spin-Spin Coupling ISO:  4.1493 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1058
          Total Spin-Spin Coupling ISO:  182.4584 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0119
          Total Spin-Spin Coupling ISO:  -0.5154 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2756
          Total Spin-Spin Coupling ISO:  6.6232 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.1062
          Total Spin-Spin Coupling ISO:  0.5564 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.6073
          Total Spin-Spin Coupling ISO:  6.6693 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2793
          Total Spin-Spin Coupling ISO:  0.2956 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 3.6113
          Total Spin-Spin Coupling ISO:  0.6349 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0343
          Total Spin-Spin Coupling ISO:  -6.9504 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4947
          Total Spin-Spin Coupling ISO:  1.4874 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.3062
          Total Spin-Spin Coupling ISO:  -0.1548 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.1843
          Total Spin-Spin Coupling ISO:  -0.3175 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.8126
          Total Spin-Spin Coupling ISO:  0.5669 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4485
          Total Spin-Spin Coupling ISO:  8.6702 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0480
          Total Spin-Spin Coupling ISO:  13.9713 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0083
          Total Spin-Spin Coupling ISO:  63.1804 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0945
          Total Spin-Spin Coupling ISO:  0.1749 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1185
          Total Spin-Spin Coupling ISO:  0.1410 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0817
          Total Spin-Spin Coupling ISO:  -0.6428 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.6046
          Total Spin-Spin Coupling ISO:  1.8234 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1320
          Total Spin-Spin Coupling ISO:  5.2710 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0898
          Total Spin-Spin Coupling ISO:  133.5506 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0932
          Total Spin-Spin Coupling ISO:  135.3247 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0891
          Total Spin-Spin Coupling ISO:  130.0699 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9377
          Total Spin-Spin Coupling ISO:  9.2261 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.5029
          Total Spin-Spin Coupling ISO:  -0.6098 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.1878
          Total Spin-Spin Coupling ISO:  -0.5438 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2721
          Total Spin-Spin Coupling ISO:  0.1322 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.4203
          Total Spin-Spin Coupling ISO:  3.8570 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.6499
          Total Spin-Spin Coupling ISO:  -0.4748 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9729
          Total Spin-Spin Coupling ISO:  9.7292 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7684
          Total Spin-Spin Coupling ISO:  -9.4715 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7656
          Total Spin-Spin Coupling ISO:  -12.9886 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7688
          Total Spin-Spin Coupling ISO:  -11.1029 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.236520466275   -0.415365053422    0.111907706978
               1 O      0.423608843652   -0.536119813111   -0.779198277748
               2 N      2.289410584921    0.437565201483    0.095977301287
               3 C      3.261080486659    0.569151423174    1.162112344709
               4 H      1.189029426024   -1.004701485620    1.046387207659
               5 H      2.370213191147    1.022986548813   -0.720972946739
               6 H      3.490552472120    1.619590951151    1.339765351244
               7 H      4.195510130602    0.049147348480    0.934916767457
               8 H      2.850974398601    0.149684879052    2.079764545154
