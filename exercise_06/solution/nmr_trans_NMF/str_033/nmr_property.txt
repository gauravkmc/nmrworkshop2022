-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1784960251
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999975396 
   Number of Beta  Electrons                 15.9999975396 
   Total number of  Electrons                31.9999950793 
   Exchange energy                          -21.4719349970 
   Correlation energy                        -1.2750660523 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7470010492 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1784960251 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8299     6.0000     0.1701     4.2223     4.2223     0.0000
  1   0     8.3577     8.0000    -0.3577     2.1365     2.1365     0.0000
  2   0     7.1911     7.0000    -0.1911     3.0473     3.0473     0.0000
  3   0     6.1772     6.0000    -0.1772     3.8559     3.8559     0.0000
  4   0     0.9399     1.0000     0.0601     1.0025     1.0025     0.0000
  5   0     0.8497     1.0000     0.1503     1.0212     1.0212    -0.0000
  6   0     0.8949     1.0000     0.1051     0.9904     0.9904     0.0000
  7   0     0.8883     1.0000     0.1117     0.9829     0.9829     0.0000
  8   0     0.8712     1.0000     0.1288     1.0053     1.0053     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.006063
                0             6               2            7                1.200836
                0             6               4            1                0.983811
                2             7               3            6                0.892328
                2             7               5            1                0.973783
                3             6               6            1                0.978109
                3             6               7            1                0.975677
                3             6               8            1                0.968401
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.178496
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.9955645624
        Electronic Contribution:
                  0    
      0      -1.648061
      1       0.401715
      2      -2.819546
        Nuclear Contribution:
                  0    
      0       1.493447
      1      -1.341921
      2       4.069791
        Total Dipole moment:
                  0    
      0      -0.154613
      1      -0.940207
      2       1.250246
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      23.951957   0.860579   4.257688
      1       3.672554  22.211506   1.025252
      2      -0.053613  -2.479128  23.555938
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.586865  -0.237015   0.774218
      1       0.673477  -0.673685   0.304264
      2       0.449464   0.699980   0.554986
 P Eigenvalues: 
                  0          1          2    
      0      19.741836  23.679952  26.297613
P(iso)  23.239800
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.733459   3.639790   0.231603
      1       2.323813  27.030966  -3.858906
      2       1.289472  -2.293345  28.012643
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.569723   0.739212   0.359139
      1       0.683801   0.183966   0.706097
      2       0.455886   0.647860  -0.610283
 P Eigenvalues: 
                  0          1          2    
      0      22.502200  28.086619  31.188248
P(iso)  27.259023
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.273260   1.296750   1.800020
      1       0.992472  25.821069   2.402767
      2       1.249971   0.787307  34.271041
 P Tensor eigenvectors:
                   0          1          2    
      0       0.304846   0.920764   0.243437
      1      -0.946605   0.264768   0.183948
      2       0.104918  -0.286514   0.952314
 P Eigenvalues: 
                  0          1          2    
      0      25.262096  28.105181  34.998093
P(iso)  29.455123
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      35.252806  -2.485713  -0.488568
      1      -2.200930  26.846846   1.126469
      2       0.619170   1.558888  25.611634
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.131149   0.223731  -0.965787
      1      -0.600490   0.757221   0.256959
      2       0.788804   0.613645   0.035040
 P Eigenvalues: 
                  0          1          2    
      0      24.568295  27.274219  35.868772
P(iso)  29.237095
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.452626   2.558338   0.877520
      1       1.705894  31.316077  -1.004373
      2       2.218274   1.001607  25.890806
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.695506  -0.618895  -0.365021
      1       0.221401   0.298705  -0.928309
      2       0.683559  -0.726460  -0.070727
 P Eigenvalues: 
                  0          1          2    
      0      24.299662  27.194230  32.165617
P(iso)  27.886503
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -9.430621 -46.247880 -62.012135
      1     -39.886011  60.449423  -5.323858
      2     -69.095586  -9.837387  13.617112
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.156014   0.826001  -0.541647
      1      -0.588739   0.362548   0.722458
      2       0.793124   0.431603   0.429736
 P Eigenvalues: 
                  0          1          2    
      0      44.679838 -70.859104  90.815181
P(iso)  21.545305
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     174.762571   8.093971   8.538501
      1       4.659494 149.678570  18.708358
      2      12.199374  18.501208 149.363519
 P Tensor eigenvectors:
                   0          1          2    
      0       0.062854   0.598462  -0.798682
      1       0.687060  -0.606387  -0.400304
      2      -0.723877  -0.523582  -0.449293
 P Eigenvalues: 
                  0          1          2    
      0     130.713726 159.263681 183.827254
P(iso)  157.934887
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     120.705215 -52.851354  29.374545
      1     -69.049858  95.747970   7.773227
      2      62.734053  26.190017 199.265012
 P Tensor eigenvectors:
                   0          1          2    
      0       0.603767   0.602646   0.521807
      1       0.730032  -0.680923  -0.058287
      2      -0.320184  -0.416128   0.851070
 P Eigenvalues: 
                  0          1          2    
      0      35.982889 159.284775 220.450533
P(iso)  138.572732
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -27.321984 -242.628016 -117.282806
      1     -244.158461  -9.006410 224.551711
      2     -96.101591 281.306667 -214.871056
 P Tensor eigenvectors:
                   0          1          2    
      0       0.788123  -0.614819  -0.029327
      1       0.331405   0.464007  -0.821504
      2       0.518684   0.637726   0.569449
 P Eigenvalues: 
                  0          1          2    
      0     -203.184468 272.570271 -320.585253
P(iso)  -83.733150
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2132
          Total Spin-Spin Coupling ISO:  30.9695 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3542
          Total Spin-Spin Coupling ISO:  10.4194 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4613
          Total Spin-Spin Coupling ISO:  -0.5309 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1045
          Total Spin-Spin Coupling ISO:  184.6485 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0297
          Total Spin-Spin Coupling ISO:  5.6780 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.3004
          Total Spin-Spin Coupling ISO:  6.2841 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.9859
          Total Spin-Spin Coupling ISO:  0.2559 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.6051
          Total Spin-Spin Coupling ISO:  3.1177 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2783
          Total Spin-Spin Coupling ISO:  0.6759 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 2.8412
          Total Spin-Spin Coupling ISO:  -0.7385 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0348
          Total Spin-Spin Coupling ISO:  -6.9132 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.1458
          Total Spin-Spin Coupling ISO:  0.4327 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.8186
          Total Spin-Spin Coupling ISO:  -0.2265 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.2372
          Total Spin-Spin Coupling ISO:  -0.1733 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.5504
          Total Spin-Spin Coupling ISO:  -0.1210 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4515
          Total Spin-Spin Coupling ISO:  7.9513 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0464
          Total Spin-Spin Coupling ISO:  14.2676 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0050
          Total Spin-Spin Coupling ISO:  64.6201 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0855
          Total Spin-Spin Coupling ISO:  0.2785 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1163
          Total Spin-Spin Coupling ISO:  -0.3340 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0763
          Total Spin-Spin Coupling ISO:  -0.6555 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.4009
          Total Spin-Spin Coupling ISO:  4.8118 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1329
          Total Spin-Spin Coupling ISO:  4.2765 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0884
          Total Spin-Spin Coupling ISO:  130.5678 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0925
          Total Spin-Spin Coupling ISO:  135.9491 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0874
          Total Spin-Spin Coupling ISO:  137.3054 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2483
          Total Spin-Spin Coupling ISO:  1.3443 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.1039
          Total Spin-Spin Coupling ISO:  -0.3849 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.9131
          Total Spin-Spin Coupling ISO:  -1.4366 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6621
          Total Spin-Spin Coupling ISO:  -1.3207 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.3685
          Total Spin-Spin Coupling ISO:  5.1714 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.7062
          Total Spin-Spin Coupling ISO:  -0.3077 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9442
          Total Spin-Spin Coupling ISO:  8.0487 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7751
          Total Spin-Spin Coupling ISO:  -9.8940 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7758
          Total Spin-Spin Coupling ISO:  -12.1567 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7656
          Total Spin-Spin Coupling ISO:  -11.8185 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.554638187788    0.176663215098   -0.522661437795
               1 O      2.044685747863    1.042908558643   -1.216519265120
               2 N      2.014928122952   -0.196216435066    0.695076808539
               3 C      3.185045636099    0.406692790406    1.306783985869
               4 H      0.660008678200   -0.395932760185   -0.825425546855
               5 H      1.542081227085   -0.946370507562    1.168015817183
               6 H      3.095391615677    0.358618397275    2.390384588435
               7 H      4.106157159795   -0.096681826363    1.004065475936
               8 H      3.248563624539    1.446638567754    0.995299573808
