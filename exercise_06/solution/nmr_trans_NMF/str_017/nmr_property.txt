-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1785585275
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999948910 
   Number of Beta  Electrons                 15.9999948910 
   Total number of  Electrons                31.9999897820 
   Exchange energy                          -21.4720420404 
   Correlation energy                        -1.2751519279 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7471939684 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1785585275 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8300     6.0000     0.1700     4.2229     4.2229     0.0000
  1   0     8.3573     8.0000    -0.3573     2.1387     2.1387    -0.0000
  2   0     7.1905     7.0000    -0.1905     3.0467     3.0467     0.0000
  3   0     6.1786     6.0000    -0.1786     3.8334     3.8334    -0.0000
  4   0     0.9407     1.0000     0.0593     1.0021     1.0021    -0.0000
  5   0     0.8509     1.0000     0.1491     1.0204     1.0204     0.0000
  6   0     0.8717     1.0000     0.1283     1.0168     1.0168     0.0000
  7   0     0.8889     1.0000     0.1111     0.9840     0.9840    -0.0000
  8   0     0.8914     1.0000     0.1086     0.9889     0.9889    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.006296
                0             6               2            7                1.202536
                0             6               4            1                0.982471
                2             7               3            6                0.882809
                2             7               5            1                0.972792
                3             6               6            1                0.967647
                3             6               7            1                0.975921
                3             6               8            1                0.977450
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.178559
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.9746826692
        Electronic Contribution:
                  0    
      0      -2.779752
      1       0.799353
      2      -1.610849
        Nuclear Contribution:
                  0    
      0       3.657461
      1      -2.086204
      2       1.748286
        Total Dipole moment:
                  0    
      0       0.877709
      1      -1.286851
      2       0.137437
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      23.726626  -2.309688   1.886451
      1       2.461982  23.895906   3.198507
      2       3.699405  -0.220035  22.183856
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.546717  -0.444822  -0.709389
      1      -0.274866   0.895611  -0.349757
      2       0.790916   0.003769  -0.611913
 P Eigenvalues: 
                  0          1          2    
      0      19.734347  23.802404  26.269636
P(iso)  23.268796
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.751417  -0.657474   2.453065
      1      -2.722353  30.414368   0.898147
      2       2.194840   2.392826  24.937427
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.555974   0.773935  -0.303179
      1      -0.286079   0.164295   0.944016
      2       0.780418   0.611581   0.130063
 P Eigenvalues: 
                  0          1          2    
      0      22.657379  28.272685  31.173148
P(iso)  27.367737
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.527002   1.952192   2.238957
      1      -0.311111  30.860001   0.080916
      2       1.719128   1.192977  25.107890
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.576940   0.785384   0.224304
      1      -0.011518  -0.282413   0.959224
      2       0.816705   0.550831   0.171981
 P Eigenvalues: 
                  0          1          2    
      0      23.714930  27.592167  31.187796
P(iso)  27.498298
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.562527   1.656842   1.443694
      1       1.078249  25.692349  -0.286462
      2       0.878952  -1.038900  35.929719
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.607745   0.786525   0.109657
      1       0.786537   0.615218  -0.053532
      2       0.109567  -0.053715   0.992527
 P Eigenvalues: 
                  0          1          2    
      0      24.560360  27.529336  36.094900
P(iso)  29.394865
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      35.479896  -0.227943  -0.176398
      1       1.420278  25.610832   0.946355
      2      -0.034435   1.645915  27.383124
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.043146  -0.017129  -0.998922
      1       0.881403   0.470108  -0.046132
      2      -0.470391   0.882443   0.005186
 P Eigenvalues: 
                  0          1          2    
      0      24.896967  28.062792  35.514093
P(iso)  29.491284
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -6.632109 -23.728205 -75.149058
      1     -16.117559  44.091081 -28.157415
      2     -72.024427 -35.704788  28.662080
 P Tensor eigenvectors:
                   0          1          2    
      0       0.562312   0.618808  -0.548527
      1      -0.825579   0.457943  -0.329709
      2       0.047168   0.638252   0.768381
 P Eigenvalues: 
                  0          1          2    
      0      45.099416 -69.701422  90.723057
P(iso)  22.040350
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     164.826173  21.074832   7.973704
      1      19.619168 142.648186   7.378470
      2       5.968287  11.242602 170.604316
 P Tensor eigenvectors:
                   0          1          2    
      0       0.485586   0.578930   0.655016
      1      -0.865496   0.212975   0.453386
      2       0.122976  -0.787072   0.604479
 P Eigenvalues: 
                  0          1          2    
      0     130.048438 162.942958 185.087279
P(iso)  159.359558
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     201.099744  -5.591095  27.429706
      1     -34.000630  57.535360 -60.712803
      2      10.964962 -38.625278 153.724661
 P Tensor eigenvectors:
                   0          1          2    
      0       0.004454  -0.520583   0.853799
      1       0.941601  -0.285298  -0.178866
      2       0.336702   0.804735   0.488911
 P Eigenvalues: 
                  0          1          2    
      0      36.840527 159.576315 215.942922
P(iso)  137.453255
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -96.267722 209.629350 -230.825109
      1     141.621115 -277.799010 -127.850793
      2     -238.318730 -113.195510 143.061356
 P Tensor eigenvectors:
                   0          1          2    
      0       0.667986  -0.671712  -0.320308
      1       0.414646  -0.021463   0.909730
      2       0.617951   0.740501  -0.264186
 P Eigenvalues: 
                  0          1          2    
      0     -194.216991 277.131648 -313.920034
P(iso)  -77.001792
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2131
          Total Spin-Spin Coupling ISO:  31.0300 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3554
          Total Spin-Spin Coupling ISO:  10.4053 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4679
          Total Spin-Spin Coupling ISO:  -0.5025 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1044
          Total Spin-Spin Coupling ISO:  185.4052 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0279
          Total Spin-Spin Coupling ISO:  5.6573 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5511
          Total Spin-Spin Coupling ISO:  4.1906 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.0890
          Total Spin-Spin Coupling ISO:  0.9033 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2580
          Total Spin-Spin Coupling ISO:  4.6613 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2832
          Total Spin-Spin Coupling ISO:  0.7434 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 2.8593
          Total Spin-Spin Coupling ISO:  -0.8033 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0333
          Total Spin-Spin Coupling ISO:  -7.0773 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.1472
          Total Spin-Spin Coupling ISO:  0.6016 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4616
          Total Spin-Spin Coupling ISO:  0.1602 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.4306
          Total Spin-Spin Coupling ISO:  -0.1447 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.7448
          Total Spin-Spin Coupling ISO:  -0.1544 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4512
          Total Spin-Spin Coupling ISO:  7.8888 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0442
          Total Spin-Spin Coupling ISO:  14.3339 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0053
          Total Spin-Spin Coupling ISO:  64.3018 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0654
          Total Spin-Spin Coupling ISO:  -0.7371 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1148
          Total Spin-Spin Coupling ISO:  -0.1482 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0946
          Total Spin-Spin Coupling ISO:  0.2437 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.4027
          Total Spin-Spin Coupling ISO:  4.7600 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1311
          Total Spin-Spin Coupling ISO:  4.0527 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0867
          Total Spin-Spin Coupling ISO:  136.1723 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0922
          Total Spin-Spin Coupling ISO:  135.0930 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0896
          Total Spin-Spin Coupling ISO:  132.2589 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2400
          Total Spin-Spin Coupling ISO:  1.4913 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6260
          Total Spin-Spin Coupling ISO:  -1.1540 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.9758
          Total Spin-Spin Coupling ISO:  -1.2352 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.0755
          Total Spin-Spin Coupling ISO:  -0.6072 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9631
          Total Spin-Spin Coupling ISO:  9.7660 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.6222
          Total Spin-Spin Coupling ISO:  -0.1047 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.4353
          Total Spin-Spin Coupling ISO:  3.3411 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7691
          Total Spin-Spin Coupling ISO:  -11.9557 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7742
          Total Spin-Spin Coupling ISO:  -12.3206 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7758
          Total Spin-Spin Coupling ISO:  -9.4270 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.246277643054    0.281833897262   -0.189342819527
               1 O      0.910039273240    1.432670140508   -0.004691875793
               2 N      2.345039981138   -0.309486948225    0.339864941014
               3 C      3.267724774092    0.379418042295    1.223035000804
               4 H      0.672518316475   -0.409844197501   -0.831246896034
               5 H      2.503371418802   -1.278362050829    0.123417728481
               6 H      3.100580768064    1.448080273396    1.118244416645
               7 H      3.109514512241    0.103469100296    2.267934479872
               8 H      4.296433312894    0.148541742799    0.947805024539
