-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1785389229
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000342823 
   Number of Beta  Electrons                 16.0000342823 
   Total number of  Electrons                32.0000685646 
   Exchange energy                          -21.4718823957 
   Correlation energy                        -1.2750998390 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7469822348 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1785389229 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8299     6.0000     0.1701     4.2225     4.2225     0.0000
  1   0     8.3573     8.0000    -0.3573     2.1373     2.1373     0.0000
  2   0     7.1910     7.0000    -0.1910     3.0470     3.0470     0.0000
  3   0     6.1784     6.0000    -0.1784     3.8454     3.8454     0.0000
  4   0     0.9403     1.0000     0.0597     1.0024     1.0024     0.0000
  5   0     0.8503     1.0000     0.1497     1.0207     1.0207     0.0000
  6   0     0.8888     1.0000     0.1112     0.9835     0.9835     0.0000
  7   0     0.8929     1.0000     0.1071     0.9899     0.9899     0.0000
  8   0     0.8710     1.0000     0.1290     1.0109     1.0109     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.006171
                0             6               2            7                1.201372
                0             6               4            1                0.983274
                2             7               3            6                0.887983
                2             7               5            1                0.973053
                3             6               6            1                0.976180
                3             6               7            1                0.977820
                3             6               8            1                0.967761
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.178539
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.9818955477
        Electronic Contribution:
                  0    
      0      -2.011538
      1       0.720235
      2      -2.515645
        Nuclear Contribution:
                  0    
      0       2.202691
      1      -1.948047
      2       3.469621
        Total Dipole moment:
                  0    
      0       0.191153
      1      -1.227812
      2       0.953975
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      23.425025  -0.157517   3.866763
      1       3.756346  23.482675   1.607372
      2       1.543279  -2.573872  22.853770
 P Tensor eigenvectors:
                   0          1          2    
      0       0.660459   0.011704   0.750771
      1      -0.397589   0.853652   0.336454
      2      -0.636959  -0.520712   0.568456
 P Eigenvalues: 
                  0          1          2    
      0      19.736660  23.744747  26.280064
P(iso)  23.253823
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      25.713412   2.647073   1.436646
      1       1.237028  29.723287  -3.156085
      2       2.857769  -1.776631  26.530100
 P Tensor eigenvectors:
                   0          1          2    
      0       0.672496  -0.725512   0.146224
      1      -0.398623  -0.188605   0.897512
      2      -0.623577  -0.661862  -0.416042
 P Eigenvalues: 
                  0          1          2    
      0      22.599917  28.191666  31.175216
P(iso)  27.322266
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.329912   0.866015  -0.025495
      1      -0.168601  27.694190   5.025979
      2      -0.392123   5.101576  32.938787
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.151257   0.988474  -0.006360
      1       0.843570   0.132432   0.520434
      2      -0.515278  -0.073355   0.853878
 P Eigenvalues: 
                  0          1          2    
      0      24.554646  27.389674  36.018569
P(iso)  29.320963
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      33.044052  -3.302981   3.631451
      1      -1.649923  28.427903  -0.055976
      2       2.988441   0.257310  26.939511
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.459249  -0.144943   0.876403
      1      -0.346659  -0.879128  -0.327049
      2       0.817874  -0.454010   0.353493
 P Eigenvalues: 
                  0          1          2    
      0      25.073799  28.102955  35.234712
P(iso)  29.470488
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.639113   4.207173  -0.565496
      1       2.343294  27.481111  -2.382653
      2       0.700173  -1.154462  26.956549
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.528027   0.482677  -0.698721
      1       0.720367  -0.181146  -0.669520
      2       0.449733   0.856860   0.252054
 P Eigenvalues: 
                  0          1          2    
      0      24.028348  27.397741  31.650684
P(iso)  27.692257
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -2.405608 -39.342892 -71.904464
      1     -33.991146  44.616150  -7.347199
      2     -75.785129 -16.024474  23.066452
 P Tensor eigenvectors:
                   0          1          2    
      0       0.126712   0.720974  -0.681279
      1      -0.817128   0.465242   0.340370
      2       0.562357   0.513563   0.648080
 P Eigenvalues: 
                  0          1          2    
      0      44.854898 -70.376376  90.798472
P(iso)  21.758998
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     173.942626   8.478214   8.113090
      1       7.760893 147.622915  17.835650
      2       8.443929  22.779393 154.275521
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.021456   0.669474  -0.742525
      1       0.760388  -0.471278  -0.446886
      2      -0.649115  -0.574196  -0.498948
 P Eigenvalues: 
                  0          1          2    
      0     130.357033 161.115094 184.368935
P(iso)  158.613687
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     157.801834 -45.504920  27.387164
      1     -71.838378  64.534074 -16.817656
      2      36.808469  12.652986 191.352704
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.400820   0.644529   0.651096
      1      -0.905227  -0.388067  -0.173112
      2       0.141093  -0.658777   0.738990
 P Eigenvalues: 
                  0          1          2    
      0      36.368220 159.344465 217.975927
P(iso)  137.896204
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0      39.452288 -89.259582 -263.407573
      1     -145.956124 -256.067212 214.266559
      2     -228.931331 223.467252 -25.088176
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.742599   0.647459  -0.171300
      1      -0.393393  -0.628680  -0.670823
      2      -0.542023  -0.430764   0.721563
 P Eigenvalues: 
                  0          1          2    
      0     -198.789378 262.277116 -305.190838
P(iso)  -80.567700
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2132
          Total Spin-Spin Coupling ISO:  31.0298 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3550
          Total Spin-Spin Coupling ISO:  10.3789 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4647
          Total Spin-Spin Coupling ISO:  -0.5172 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1044
          Total Spin-Spin Coupling ISO:  184.8825 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0286
          Total Spin-Spin Coupling ISO:  5.6344 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.0332
          Total Spin-Spin Coupling ISO:  0.4454 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2843
          Total Spin-Spin Coupling ISO:  5.6553 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5765
          Total Spin-Spin Coupling ISO:  3.6557 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2813
          Total Spin-Spin Coupling ISO:  0.7250 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 2.8510
          Total Spin-Spin Coupling ISO:  -0.7484 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0338
          Total Spin-Spin Coupling ISO:  -6.9805 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.1464
          Total Spin-Spin Coupling ISO:  0.5316 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.3287
          Total Spin-Spin Coupling ISO:  -0.1658 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.7904
          Total Spin-Spin Coupling ISO:  -0.1810 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.5036
          Total Spin-Spin Coupling ISO:  0.0473 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4513
          Total Spin-Spin Coupling ISO:  7.9337 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0454
          Total Spin-Spin Coupling ISO:  14.2968 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0052
          Total Spin-Spin Coupling ISO:  64.4030 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1167
          Total Spin-Spin Coupling ISO:  -0.3111 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0894
          Total Spin-Spin Coupling ISO:  0.3046 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0699
          Total Spin-Spin Coupling ISO:  -0.6794 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.4018
          Total Spin-Spin Coupling ISO:  4.7951 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1319
          Total Spin-Spin Coupling ISO:  4.1706 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0925
          Total Spin-Spin Coupling ISO:  135.4670 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0889
          Total Spin-Spin Coupling ISO:  131.4446 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0870
          Total Spin-Spin Coupling ISO:  136.6544 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2439
          Total Spin-Spin Coupling ISO:  1.4220 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.9406
          Total Spin-Spin Coupling ISO:  -1.3532 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.0936
          Total Spin-Spin Coupling ISO:  -0.4742 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6433
          Total Spin-Spin Coupling ISO:  -1.2365 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.6610
          Total Spin-Spin Coupling ISO:  -0.3341 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.4024
          Total Spin-Spin Coupling ISO:  4.2048 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9566
          Total Spin-Spin Coupling ISO:  9.1272 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7763
          Total Spin-Spin Coupling ISO:  -9.6133 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7667
          Total Spin-Spin Coupling ISO:  -11.9486 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7750
          Total Spin-Spin Coupling ISO:  -12.3378 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.453870287546    0.263481324213   -0.436359224375
               1 O      1.658142760225    1.369504109120   -0.891055303313
               2 N      2.140496263568   -0.300141135815    0.586828047328
               3 C      3.210120326372    0.385262323606    1.288600798430
               4 H      0.672203048809   -0.408421271538   -0.832980934794
               5 H      1.849982836534   -1.209837279400    0.900642522941
               6 H      2.847001167947    0.898308477102    2.182182877867
               7 H      3.980042489804   -0.328632932579    1.577261065600
               8 H      3.639640819195    1.126796385293    0.619900150315
