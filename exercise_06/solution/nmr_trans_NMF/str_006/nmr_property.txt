-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1785781988
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999911802 
   Number of Beta  Electrons                 15.9999911802 
   Total number of  Electrons                31.9999823604 
   Exchange energy                          -21.4721164526 
   Correlation energy                        -1.2751809169 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7472973694 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1785781988 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8303     6.0000     0.1697     4.2232     4.2232    -0.0000
  1   0     8.3569     8.0000    -0.3569     2.1400     2.1400     0.0000
  2   0     7.1903     7.0000    -0.1903     3.0463     3.0463     0.0000
  3   0     6.1793     6.0000    -0.1793     3.8256     3.8256     0.0000
  4   0     0.9409     1.0000     0.0591     1.0018     1.0018    -0.0000
  5   0     0.8512     1.0000     0.1488     1.0202     1.0202     0.0000
  6   0     0.8904     1.0000     0.1096     0.9879     0.9879    -0.0000
  7   0     0.8719     1.0000     0.1281     1.0208     1.0208     0.0000
  8   0     0.8888     1.0000     0.1112     0.9848     0.9848    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.006741
                0             6               2            7                1.202881
                0             6               4            1                0.981933
                2             7               3            6                0.879497
                2             7               5            1                0.972535
                3             6               6            1                0.977112
                3             6               7            1                0.967534
                3             6               8            1                0.975853
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.178578
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.9690103280
        Electronic Contribution:
                  0    
      0      -3.233263
      1       0.093967
      2      -0.743231
        Nuclear Contribution:
                  0    
      0       4.504580
      1      -0.751205
      2       0.118680
        Total Dipole moment:
                  0    
      0       1.271317
      1      -0.657239
      2      -0.624552
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      24.710038  -1.663001  -1.086599
      1       1.661608  21.421289   2.961803
      2       3.950536   1.951195  23.705580
 P Tensor eigenvectors:
                   0          1          2    
      0       0.159812   0.716619  -0.678909
      1       0.813454  -0.485228  -0.320697
      2      -0.559243  -0.501010  -0.660482
 P Eigenvalues: 
                  0          1          2    
      0      19.728546  23.850190  26.258171
P(iso)  23.278969
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.013218  -0.749820   0.484089
      1      -2.396416  25.316696   3.190797
      2      -1.487143   3.819405  27.879723
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.159864   0.814457  -0.557767
      1      -0.823371   0.201666   0.530464
      2       0.544522   0.544051   0.638361
 P Eigenvalues: 
                  0          1          2    
      0      22.701321  28.335291  31.173024
P(iso)  27.403212
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      32.225972   4.543659  -0.292330
      1       5.628078  28.337204  -1.252595
      2       0.164998  -0.013716  27.930458
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.548236   0.131293  -0.825954
      1       0.823900  -0.084808  -0.560354
      2       0.143618   0.987709   0.061678
 P Eigenvalues: 
                  0          1          2    
      0      24.763332  27.983731  35.746571
P(iso)  29.497878
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.555913  -0.120753   1.336677
      1      -1.721601  27.052849   3.444436
      2      -0.660278   3.696441  27.507360
 P Tensor eigenvectors:
                   0          1          2    
      0       0.219195   0.965675  -0.139378
      1       0.721324  -0.064197   0.689617
      2      -0.656998   0.251697   0.710635
 P Eigenvalues: 
                  0          1          2    
      0      23.500385  27.713997  30.901739
P(iso)  27.372041
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.711235  -1.167796   4.436019
      1      -1.319434  29.204337  -2.655496
      2       4.447056  -3.880426  31.425217
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.768314   0.433803   0.470648
      1       0.211122   0.865906  -0.453470
      2       0.604253   0.249043   0.756873
 P Eigenvalues: 
                  0          1          2    
      0      24.579476  27.677049  36.084264
P(iso)  29.446930
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -16.401183 -24.221073 -65.041128
      1     -18.373146  68.321656 -38.294424
      2     -55.797974 -41.104680  14.724972
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.805884   0.571522   0.154641
      1       0.415150   0.359233   0.835824
      2       0.422140   0.737777  -0.526767
 P Eigenvalues: 
                  0          1          2    
      0      45.192058 -69.266352  90.719739
P(iso)  22.215148
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     151.534233  18.117960  21.880000
      1      17.052899 159.243267  -2.044221
      2      18.279882  -0.373739 168.783736
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.776521  -0.183941  -0.602644
      1       0.479991  -0.792303  -0.376650
      2       0.408195   0.581741  -0.703530
 P Eigenvalues: 
                  0          1          2    
      0     129.872182 164.153964 185.535091
P(iso)  159.853746
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     210.610602  18.814467  42.725534
      1       0.791355 119.003532 -60.392672
      2       8.831165 -54.214711  81.937926
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.224424   0.134254  -0.965199
      1       0.579107   0.814974  -0.021294
      2       0.783753  -0.563732  -0.260647
 P Eigenvalues: 
                  0          1          2    
      0      37.214993 159.772309 214.564757
P(iso)  137.184020
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -304.689699 155.827822  46.003173
      1     105.414408 141.242438 -276.369387
      2      -4.443826 -277.512925 -60.489239
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.616121  -0.322884  -0.718429
      1      -0.355031  -0.700358   0.619235
      2      -0.703099   0.636588   0.316872
 P Eigenvalues: 
                  0          1          2    
      0     -190.874526 310.477811 -343.539784
P(iso)  -74.645500
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2129
          Total Spin-Spin Coupling ISO:  31.0425 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3558
          Total Spin-Spin Coupling ISO:  10.3565 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4702
          Total Spin-Spin Coupling ISO:  -0.4865 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1043
          Total Spin-Spin Coupling ISO:  185.7315 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0271
          Total Spin-Spin Coupling ISO:  5.6127 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2293
          Total Spin-Spin Coupling ISO:  3.6916 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5370
          Total Spin-Spin Coupling ISO:  4.5567 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.1356
          Total Spin-Spin Coupling ISO:  1.5561 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2851
          Total Spin-Spin Coupling ISO:  0.7686 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 2.8659
          Total Spin-Spin Coupling ISO:  -0.7883 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0322
          Total Spin-Spin Coupling ISO:  -7.1227 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.1476
          Total Spin-Spin Coupling ISO:  0.6557 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.6940
          Total Spin-Spin Coupling ISO:  -0.1288 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4385
          Total Spin-Spin Coupling ISO:  0.2935 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.5187
          Total Spin-Spin Coupling ISO:  -0.1502 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4510
          Total Spin-Spin Coupling ISO:  7.9264 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0439
          Total Spin-Spin Coupling ISO:  14.3438 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0054
          Total Spin-Spin Coupling ISO:  64.2593 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0999
          Total Spin-Spin Coupling ISO:  0.1633 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0629
          Total Spin-Spin Coupling ISO:  -0.7336 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1111
          Total Spin-Spin Coupling ISO:  -0.0364 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.4035
          Total Spin-Spin Coupling ISO:  4.7555 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1305
          Total Spin-Spin Coupling ISO:  4.0060 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0903
          Total Spin-Spin Coupling ISO:  132.9240 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0866
          Total Spin-Spin Coupling ISO:  136.2455 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0919
          Total Spin-Spin Coupling ISO:  134.1685 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2375
          Total Spin-Spin Coupling ISO:  1.5338 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.0576
          Total Spin-Spin Coupling ISO:  -0.7426 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6170
          Total Spin-Spin Coupling ISO:  -1.1034 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.0039
          Total Spin-Spin Coupling ISO:  -1.1132 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.4705
          Total Spin-Spin Coupling ISO:  2.4724 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9680
          Total Spin-Spin Coupling ISO:  10.2570 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.5821
          Total Spin-Spin Coupling ISO:  0.3376 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7749
          Total Spin-Spin Coupling ISO:  -12.2350 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7748
          Total Spin-Spin Coupling ISO:  -9.4170 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7704
          Total Spin-Spin Coupling ISO:  -12.0407 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.124009163543    0.086778222667    0.047912078504
               1 O      0.473785062632    0.743910769494    0.833149543556
               2 N      2.462761059681   -0.118154285163    0.110438903977
               3 C      3.304893479144    0.435520506610    1.154317506941
               4 H      0.668961074860   -0.418994646771   -0.821901005734
               5 H      2.879847020866   -0.687011631849   -0.605931687710
               6 H      4.160071303190    0.956094618091    0.722518934355
               7 H      2.707919473684    1.140670064247    1.726237335756
               8 H      3.669252362399   -0.342493617326    1.828278390355
