-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1785430563
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000041233 
   Number of Beta  Electrons                 16.0000041233 
   Total number of  Electrons                32.0000082465 
   Exchange energy                          -21.4720887750 
   Correlation energy                        -1.2751334635 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7472222385 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1785430563 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8299     6.0000     0.1701     4.2225     4.2225     0.0000
  1   0     8.3574     8.0000    -0.3574     2.1376     2.1376    -0.0000
  2   0     7.1907     7.0000    -0.1907     3.0469     3.0469     0.0000
  3   0     6.1785     6.0000    -0.1785     3.8406     3.8406     0.0000
  4   0     0.9405     1.0000     0.0595     1.0022     1.0022     0.0000
  5   0     0.8505     1.0000     0.1495     1.0206     1.0206     0.0000
  6   0     0.8923     1.0000     0.1077     0.9895     0.9895     0.0000
  7   0     0.8713     1.0000     0.1287     1.0134     1.0134     0.0000
  8   0     0.8889     1.0000     0.1111     0.9837     0.9837     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.005922
                0             6               2            7                1.202021
                0             6               4            1                0.982954
                2             7               3            6                0.885841
                2             7               5            1                0.972908
                3             6               6            1                0.977688
                3             6               7            1                0.967750
                3             6               8            1                0.976115
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.178543
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.9780534644
        Electronic Contribution:
                  0    
      0      -3.231847
      1      -0.015654
      2      -0.683216
        Nuclear Contribution:
                  0    
      0       4.516600
      1      -0.551816
      2      -0.007281
        Total Dipole moment:
                  0    
      0       1.284753
      1      -0.567470
      2      -0.690497
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      24.760226  -1.353923  -1.309945
      1       1.658535  21.121653   2.682846
      2       4.022173   1.974004  23.892655
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.110771   0.713886  -0.691445
      1      -0.851574  -0.426874  -0.304304
      2       0.512398  -0.555109  -0.655212
 P Eigenvalues: 
                  0          1          2    
      0      19.736629  23.765716  26.272189
P(iso)  23.258178
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.050096  -0.587685   0.136354
      1      -2.215226  24.777466   3.112943
      2      -1.669549   3.588000  28.182856
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.122739   0.808190   0.575989
      1      -0.856581   0.206833  -0.472747
      2       0.501203   0.551406  -0.666894
 P Eigenvalues: 
                  0          1          2    
      0      22.620058  28.216076  31.174284
P(iso)  27.336806
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      32.904429   3.745656  -0.258289
      1       5.022676  27.468692  -0.858588
      2       0.355253   0.260373  28.066297
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.474155   0.060704  -0.878346
      1       0.876379  -0.063188  -0.477459
      2       0.084485   0.996154   0.023239
 P Eigenvalues: 
                  0          1          2    
      0      25.003273  28.091624  35.344521
P(iso)  29.479806
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.222350   0.093485   1.552637
      1      -1.574522  28.416930   3.607236
      2      -0.401663   3.585540  27.176542
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.267801   0.961013  -0.068828
      1      -0.631131  -0.121000   0.766181
      2       0.727982   0.248623   0.638928
 P Eigenvalues: 
                  0          1          2    
      0      23.894468  27.467846  31.453508
P(iso)  27.605274
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.036918  -0.899849   4.313490
      1      -0.981633  28.964557  -3.054098
      2       3.743726  -3.963930  32.058569
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.793792   0.458596   0.399479
      1       0.263733   0.851417  -0.453358
      2       0.548032   0.254516   0.796796
 P Eigenvalues: 
                  0          1          2    
      0      24.553657  27.444580  36.061807
P(iso)  29.353348
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -17.553866 -24.889110 -63.309040
      1     -20.421068  71.360026 -37.345167
      2     -53.730568 -40.276319  11.769957
 P Tensor eigenvectors:
                   0          1          2    
      0       0.806020  -0.584489   0.093294
      1      -0.339366  -0.327225   0.881904
      2      -0.484935  -0.742493  -0.462106
 P Eigenvalues: 
                  0          1          2    
      0      44.948888 -70.185070  90.812299
P(iso)  21.858706
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     150.998393  16.039329  22.559854
      1      16.099115 158.136009  -1.653052
      2      18.319514   0.777827 167.690313
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.781508  -0.178636  -0.597775
      1       0.458047  -0.814818  -0.355337
      2       0.423602   0.551507  -0.718611
 P Eigenvalues: 
                  0          1          2    
      0     130.283718 161.858132 184.682865
P(iso)  158.941572
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     213.251501  19.090798  45.574223
      1       4.831475 126.363691 -55.324694
      2       8.284172 -52.037954  73.649844
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.236836   0.078309  -0.968389
      1       0.519332   0.852598  -0.058066
      2       0.821099  -0.516667  -0.242594
 P Eigenvalues: 
                  0          1          2    
      0      36.499661 159.537155 217.228220
P(iso)  137.755012
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -313.048683 122.476846  71.163714
      1      69.554268 187.050132 -257.089300
      2      31.622978 -274.700766 -111.736338
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.641523  -0.342932  -0.686182
      1      -0.323438  -0.690189   0.647323
      2      -0.695583   0.637210   0.331855
 P Eigenvalues: 
                  0          1          2    
      0     -197.259324 263.788535 -304.264100
P(iso)  -79.244963
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2131
          Total Spin-Spin Coupling ISO:  31.0033 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3551
          Total Spin-Spin Coupling ISO:  10.3610 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4654
          Total Spin-Spin Coupling ISO:  -0.5274 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1044
          Total Spin-Spin Coupling ISO:  185.3341 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0285
          Total Spin-Spin Coupling ISO:  5.6085 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2750
          Total Spin-Spin Coupling ISO:  5.2931 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5653
          Total Spin-Spin Coupling ISO:  3.8705 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.0540
          Total Spin-Spin Coupling ISO:  0.5907 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2817
          Total Spin-Spin Coupling ISO:  0.7332 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 2.8529
          Total Spin-Spin Coupling ISO:  -0.7530 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0335
          Total Spin-Spin Coupling ISO:  -7.0276 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.1466
          Total Spin-Spin Coupling ISO:  0.5581 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.7737
          Total Spin-Spin Coupling ISO:  -0.1557 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4842
          Total Spin-Spin Coupling ISO:  0.1207 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.3664
          Total Spin-Spin Coupling ISO:  -0.1636 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4512
          Total Spin-Spin Coupling ISO:  7.9183 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0452
          Total Spin-Spin Coupling ISO:  14.2852 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0052
          Total Spin-Spin Coupling ISO:  64.4039 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0913
          Total Spin-Spin Coupling ISO:  0.2931 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0683
          Total Spin-Spin Coupling ISO:  -0.6887 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1159
          Total Spin-Spin Coupling ISO:  -0.2563 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.4020
          Total Spin-Spin Coupling ISO:  4.7599 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1317
          Total Spin-Spin Coupling ISO:  4.1076 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0892
          Total Spin-Spin Coupling ISO:  131.9068 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0869
          Total Spin-Spin Coupling ISO:  136.8047 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0924
          Total Spin-Spin Coupling ISO:  135.0530 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2432
          Total Spin-Spin Coupling ISO:  1.4263 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.0875
          Total Spin-Spin Coupling ISO:  -0.5196 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6355
          Total Spin-Spin Coupling ISO:  -1.2103 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.9542
          Total Spin-Spin Coupling ISO:  -1.3206 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.4139
          Total Spin-Spin Coupling ISO:  3.9061 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9594
          Total Spin-Spin Coupling ISO:  9.3937 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.6470
          Total Spin-Spin Coupling ISO:  -0.2744 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7755
          Total Spin-Spin Coupling ISO:  -12.2786 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7754
          Total Spin-Spin Coupling ISO:  -9.5483 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7675
          Total Spin-Spin Coupling ISO:  -11.9207 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.121902437997    0.056795009116    0.065486635978
               1 O      0.465589105187    0.631745342016    0.908218487096
               2 N      2.470012196736   -0.079426341319    0.082042882592
               3 C      3.302080369449    0.444587923048    1.149305529386
               4 H      0.667117426781   -0.415041367956   -0.823452556435
               5 H      2.896304792519   -0.605601795560   -0.660836800618
               6 H      4.229668871440    0.840675368856    0.738261615389
               7 H      2.757614552364    1.243882401043    1.645345605670
               8 H      3.541210247526   -0.321296539244    1.890648600941
