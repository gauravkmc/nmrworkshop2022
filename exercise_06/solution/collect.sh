# bash collect.sh > J_cis_trans.csv
for f in $(seq -f "%03g" 1 1 36); do
  Jcis=$( grep '      6 H    ' nmr_cis_NMF/str_$f/nmr.out | head -1 | awk '{print $8}' )
  Jtrans=$( grep '      6 H    ' nmr_trans_NMF/str_$f/nmr.out | head -1 | awk '{print $8}' )
  echo $Jcis","$Jtrans
done
