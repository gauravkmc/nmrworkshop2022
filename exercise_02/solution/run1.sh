obabel -oxyz NMF.smi > NMF.xyz  --gen3d
obabel -osvg NMF.smi > NMF.svg
obminimize -oxyz -sd -ff uff NMF.xyz > NMF_UFF.xyz
obminimize -oxyz -sd -mmff94 uff NMF.xyz > NMF_MMFF94.xyz

