obminimize -oxyz -n 10000 -c 1e-6 -sd -ff uff NMF_conformers.xyz > NMF_conformers_UFF.xyz
obminimize -oxyz -n 10000 -c 1e-6 -sd -ff mmff94 NMF_conformers.xyz > NMF_conformers_MMFF94.xyz
