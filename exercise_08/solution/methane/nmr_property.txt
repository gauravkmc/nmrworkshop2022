-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:      -40.5032800335
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                  4.9999998619 
   Number of Beta  Electrons                  4.9999998619 
   Total number of  Electrons                 9.9999997238 
   Exchange energy                           -5.2097476801 
   Correlation energy                        -0.3514538893 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy               -5.5612015694 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -40.5032800335 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 5
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 4
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.4068     6.0000    -0.4068     3.9204     3.9204     0.0000
  1   0     0.8983     1.0000     0.1017     0.9939     0.9939    -0.0000
  2   0     0.8983     1.0000     0.1017     0.9938     0.9938     0.0000
  3   0     0.8983     1.0000     0.1017     0.9939     0.9939     0.0000
  4   0     0.8982     1.0000     0.1018     0.9938     0.9938     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            1                0.980116
                0             6               2            1                0.980080
                0             6               3            1                0.980128
                0             6               4            1                0.980082
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       5
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             87
     number of aux C basis functions:       0
     number of aux J basis functions:       93
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -40.503280
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000970960
        Electronic Contribution:
                  0    
      0      -0.000056
      1       0.000175
      2       0.000019
        Nuclear Contribution:
                  0    
      0       0.000071
      1      -0.000210
      2      -0.000024
        Total Dipole moment:
                  0    
      0       0.000015
      1      -0.000035
      2      -0.000005
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 5
 Source density: 1 SCF 
 Nucleus: 1 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      37.590454  -0.001481  -0.000215
      1      -0.000258  28.437558  -0.000196
      2      -0.000633  -0.000033  28.437579
 P Tensor eigenvectors:
                   0          1          2    
      0       0.000106  -0.000038  -1.000000
      1       0.739000  -0.673705   0.000104
      2       0.673705   0.739000   0.000043
 P Eigenvalues: 
                  0          1          2    
      0      28.437453  28.437683  37.590454
P(iso)  31.488530
 Nucleus: 2 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.457431  -2.697341  -0.995801
      1      -2.692659  35.600594   2.641825
      2      -0.993941   2.642555  29.420208
 P Tensor eigenvectors:
                   0          1          2    
      0       0.938767  -0.088453  -0.333006
      1       0.343507   0.315529   0.884559
      2       0.026831  -0.944784   0.326592
 P Eigenvalues: 
                  0          1          2    
      0      28.442737  28.444651  37.590845
P(iso)  31.492744
 Nucleus: 3 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.455895   2.212158  -1.837805
      1       2.206448  33.252557  -3.997046
      2      -1.835441  -3.997261  31.765978
 P Tensor eigenvectors:
                   0          1          2    
      0       0.939922  -0.075180  -0.333009
      1      -0.204314   0.657595  -0.725137
      2       0.273501   0.749611   0.602728
 P Eigenvalues: 
                  0          1          2    
      0      28.441081  28.443608  37.589740
P(iso)  31.491476
 Nucleus: 4 H  
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.455770   0.486533   2.833979
      1       0.485242  28.676647   1.357097
      2       2.829497   1.356495  36.342329
 P Tensor eigenvectors:
                   0          1          2    
      0       0.927664   0.168950   0.333008
      1       0.121135  -0.979720   0.159610
      2      -0.353221   0.107725   0.929317
 P Eigenvalues: 
                  0          1          2    
      0      28.440878  28.443675  37.590192
P(iso)  31.491582
 Nucleus: 0 C  
 Shielding tensor (ppm): 
                  0          1          2    
      0     190.806760   0.005384  -0.006256
      1       0.011525 190.803258   0.000601
      2      -0.003337  -0.000985 190.811663
 P Tensor eigenvectors:
                   0          1          2    
      0       0.650993   0.384346  -0.654588
      1      -0.735382   0.533131  -0.418311
      2       0.188205   0.753691   0.629706
 P Eigenvalues: 
                  0          1          2    
      0     190.795823 190.809081 190.816777
P(iso)  190.807227
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     1 
    Coordinates:
               0 C      1.060183854607   -0.068670321069   -0.040631192070
               1 H      2.148791511569   -0.068748277281   -0.040628740921
               2 H      0.697352892511    0.894123315239    0.314794484633
               3 H      0.697362001019   -0.857984331887    0.615452127325
               4 H      0.697329740293   -0.242370385002   -1.052176678967
