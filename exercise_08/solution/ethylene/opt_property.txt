-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:      -78.5668541812
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                  7.9999924808 
   Number of Beta  Electrons                  7.9999924808 
   Total number of  Electrons                15.9999849615 
   Exchange energy                           -9.3114107024 
   Correlation energy                        -0.5916323414 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy               -9.9030430438 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -78.5668541812 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 6
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 5
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.2142     6.0000    -0.2142     3.8812     3.8812     0.0000
  1   0     6.2142     6.0000    -0.2142     3.8812     3.8812     0.0000
  2   0     0.8929     1.0000     0.1071     0.9979     0.9979     0.0000
  3   0     0.8929     1.0000     0.1071     0.9979     0.9979    -0.0000
  4   0     0.8929     1.0000     0.1071     0.9979     0.9979     0.0000
  5   0     0.8929     1.0000     0.1071     0.9979     0.9979    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.899992
                0             6               2            1                0.979867
                0             6               3            1                0.979864
                1             6               4            1                0.979861
                1             6               5            1                0.979865
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0039519427
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:      -78.5671312399
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                  7.9999937372 
   Number of Beta  Electrons                  7.9999937372 
   Total number of  Electrons                15.9999874743 
   Exchange energy                           -9.3159871842 
   Correlation energy                        -0.5918907673 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy               -9.9078779515 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -78.5671312399 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0039511927
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:      -78.5673254819
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                  7.9999956552 
   Number of Beta  Electrons                  7.9999956552 
   Total number of  Electrons                15.9999913103 
   Exchange energy                           -9.3181067283 
   Correlation energy                        -0.5919975836 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy               -9.9101043119 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -78.5673254819 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0039468061
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:      -78.5673316132
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                  7.9999957752 
   Number of Beta  Electrons                  7.9999957752 
   Total number of  Electrons                15.9999915503 
   Exchange energy                           -9.3170043187 
   Correlation energy                        -0.5919301272 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy               -9.9089344458 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -78.5673316132 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0039456955
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:      -78.5673319675
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                  7.9999957710 
   Number of Beta  Electrons                  7.9999957710 
   Total number of  Electrons                15.9999915420 
   Exchange energy                           -9.3166923866 
   Correlation energy                        -0.5919113409 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy               -9.9086037275 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -78.5673319675 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 5
   prop. index: 1
     Number of atoms                     : 6
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 5
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.2140     6.0000    -0.2140     3.8882     3.8882     0.0000
  1   0     6.2141     6.0000    -0.2141     3.8882     3.8882     0.0000
  2   0     0.8930     1.0000     0.1070     0.9964     0.9964    -0.0000
  3   0     0.8930     1.0000     0.1070     0.9964     0.9964     0.0000
  4   0     0.8930     1.0000     0.1070     0.9964     0.9964     0.0000
  5   0     0.8930     1.0000     0.1070     0.9964     0.9964     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.908112
                0             6               2            1                0.978528
                0             6               3            1                0.978526
                1             6               4            1                0.978523
                1             6               5            1                0.978527
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0039455009
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 5
   prop. index: 1
       Filename                          : opt.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000286861
        Electronic Contribution:
                  0    
      0      -0.000013
      1      -0.000000
      2      -0.000000
        Nuclear Contribution:
                  0    
      0       0.000001
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0      -0.000011
      1      -0.000000
      2       0.000000
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    6 
    Geometry Index:     1 
    Coordinates:
               0 C      1.049980000000   -0.079800000000   -0.048820000000
               1 C      2.381100000000   -0.079800000000   -0.048820000000
               2 H      0.503450000000   -0.560100000000   -0.853940000000
               3 H      0.503450000000    0.400500000000    0.756310000000
               4 H      2.927640000000   -0.560100000000   -0.853940000000
               5 H      2.927640000000    0.400500000000    0.756310000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    6 
    Geometry Index:     2 
    Coordinates:
               0 C      1.051322245060   -0.079800525687   -0.048817102614
               1 C      2.379760771527   -0.079800503157   -0.048817120882
               2 H      0.498216058971   -0.556332371541   -0.847626251107
               3 H      0.498218743686    0.396732895056    0.749993362704
               4 H      2.932872440069   -0.556330987064   -0.847623857160
               5 H      2.932869740688    0.396731492392    0.749990969058
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    6 
    Geometry Index:     3 
    Coordinates:
               0 C      1.053107840079   -0.079799893757   -0.048816110934
               1 C      2.377979119520   -0.079799900624   -0.048816120733
               2 H      0.485942695973   -0.551644215863   -0.839767479525
               3 H      0.485944634494    0.392044118468    0.742133604599
               4 H      2.945143842301   -0.551642893653   -0.839765220543
               5 H      2.945141867633    0.392042785429    0.742131327136
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    6 
    Geometry Index:     4 
    Coordinates:
               0 C      1.053103953790   -0.079800063184   -0.048816521946
               1 C      2.377982361551   -0.079800060289   -0.048816541859
               2 H      0.484055050516   -0.551615778660   -0.839719746583
               3 H      0.484055583452    0.392015848098    0.742086284427
               4 H      2.947031808972   -0.551615957344   -0.839719990844
               5 H      2.947031241720    0.392016011380    0.742086516805
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    6 
    Geometry Index:     5 
    Coordinates:
               0 C      1.053064107493   -0.079800004407   -0.048816693933
               1 C      2.378022051319   -0.079800014632   -0.048816700837
               2 H      0.483812593722   -0.551695546059   -0.839853317706
               3 H      0.483812767519    0.392095555788    0.742220017886
               4 H      2.947274333684   -0.551695927222   -0.839853933676
               5 H      2.947274146263    0.392095936533    0.742220628266
