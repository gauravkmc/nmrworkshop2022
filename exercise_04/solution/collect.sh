Natm=17
grep -A$(( $Natm+5 )) 'CHEMICAL SHIELDING SUMMARY' tms/nmr/nmr.out  | tail -$(( $Natm+2 ))

Natm=9
grep -A$(( $Natm+5 )) 'CHEMICAL SHIELDING SUMMARY' cis_NMF/nmr/nmr.out  | tail -$(( $Natm+2 ))

echo""
echo "1H shielding"
H=$( grep -A$(( $Natm+5 )) 'CHEMICAL SHIELDING SUMMARY' cis_NMF/nmr/nmr.out  | tail -$(( $Natm+2 )) | grep 'H' | awk '{print $3}' )
echo $H
echo""
echo "13C shielding"
C=$( grep -A$(( $Natm+5 )) 'CHEMICAL SHIELDING SUMMARY' cis_NMF/nmr/nmr.out  | tail -$(( $Natm+2 )) | grep 'C' | awk '{print $3}' )
echo $C

Natm=9
grep -A$(( $Natm+5 )) 'CHEMICAL SHIELDING SUMMARY' trans_NMF/nmr/nmr.out  | tail -$(( $Natm+2 ))

echo""
echo "1H shielding"
H=$( grep -A$(( $Natm+5 )) 'CHEMICAL SHIELDING SUMMARY' trans_NMF/nmr/nmr.out  | tail -$(( $Natm+2 )) | grep 'H' | awk '{print $3}' )
echo $H
echo""
echo "13C shielding"
C=$( grep -A$(( $Natm+5 )) 'CHEMICAL SHIELDING SUMMARY' trans_NMF/nmr/nmr.out  | tail -$(( $Natm+2 )) | grep 'C' | awk '{print $3}' )
echo $C
