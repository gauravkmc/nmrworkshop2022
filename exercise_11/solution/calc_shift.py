import numpy as np

# Isotropic shielding of TMS
tms_H=31.7

# C18H18
H=[ 42.684,42.695,42.695,42.684,42.695,42.695,20.060,20.060,20.059,20.058,20.060,20.060,20.060,20.060,20.059,20.059,20.060,20.060]
delta = tms_H - np.array(H)
print(delta)

